FROM ubuntu:20.04


# Install dependencies
RUN apt-get update && apt-get install -y \
        software-properties-common
    RUN add-apt-repository universe
    RUN apt-get update && apt-get install -y \
        python3.9 \
        python3-pip
    RUN python3.9 -m pip install pip
    RUN apt-get update && apt-get install -y \
        python3-distutils \
        python3-setuptools
    RUN apt-get update && apt-get install -y \
        default-libmysqlclient-dev
    RUN apt-get install -y cron

        

WORKDIR /django

RUN mkdir staticfiles

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

COPY requirements.txt requirements.txt

# RUN python -m pip install --upgrade pip

RUN pip3 install -r requirements.txt


# copy entrypoint.sh
COPY entrypoint.sh entrypoint.sh

RUN chmod +x entrypoint.sh



# run entrypoint.prod.sh
#ENTRYPOINT ["/django/entrypoint.sh"]


# Copy hello-cron file to the cron.d directory

COPY my-crontab /etc/cron.d/my-crontab
COPY service/crowling.py crowling.py

# Give execution rights on the cron job
RUN chmod 0644 /etc/cron.d/my-crontab
RUN chmod a+x crowling.py

RUN crontab /etc/cron.d/my-crontab

# Create the log file to be able to run tail
RUN touch /var/log/cron.log
CMD ["cron", "-f"]