# SNA Project

## Contents
- [Introduction](#intro)
- [Solution plan](#sol)
- [Solution](#test)
- [Difficulties](#diff)
- [Video Demonstration](#video)
- [Results](#res) 
- [Conclusion](#conc) 
- [Contribution](#cont)
- [Running](#runn) 

## Introduction <a id="intro"> </a>
This project is done as the final assignment of *SNA* course taken in Fall 2022 semester at Innopolis University

### Goal
* The goal of this project is to deploy a web application which is written in [Django](https://www.djangoproject.com/) and [MySQL](https://www.mysql.com/) database and backgroundjob via Celery & Celery-beat & Redis to scrap Statistic website [Worldometers](https://www.worldometers.info/) to get data and contantly update our database for web service about World Population statistics.
* Application shows world population by country and by year.
* It has filters like sorting by any collumns of created table.
* Such as population, country_name, growth rate and etc. 

To know more about the web-app - [Click Here](https://gitlab.com/rametago/populationinfo)

### Tasks
1. Make web service and deploy it using docker and docker-compose on nginx and gunicorn
2. Store Docker container logs to rsyslog 
3. Implement CI/CD by using Git and Gitlab


## Solution Plan <a id="sol"> </a>
### Methodology
- We used Docker Compose over Virtual machine, for deploying our project.

- One of the benefits of Docker Compose over Virtial manchine is that it has one kernel which is used for every docker container, which makes them weight as a executable.

- With only one command, we can run whole application on anywhere docker exists.

Therefore, it was more efficient for us to use *Docker*

### Execution plan
- We decided to create 2 separate *Dockerfiles* for application itself and for nginx. 
- Followed by a *docker-compose* file for defining and running multi-containers. 
- To check all logs of images we used configured that all docker logs goes to rsyslog. 
- To test whole application and make easier development we implemented CI/CD pipeline.

## Solution <a id="test"> </a>

 ### Dockerizing the NGINX
- You will need to install Docker  - [link](https://docs.docker.com/engine/install/ubuntu/)

 #### Creating the Dockerfile
```dockerfile
FROM nginx:1.21-alpine

COPY ./default.conf /etc/nginx/conf.d/default.conf
```
### Dockerizing the application
- Similar to the nginx.
#### Creating the dockerfile
```dockerfile
FROM ubuntu:20.04

# Install dependencies
RUN apt-get update && apt-get install -y \
        software-properties-common
    RUN add-apt-repository universe
    RUN apt-get update && apt-get install -y \
        python3.9 \
        python3-pip
    RUN python3.9 -m pip install pip
    RUN apt-get update && apt-get install -y \
        python3-distutils \
        python3-setuptools
    RUN apt-get update && apt-get install -y \
        default-libmysqlclient-dev
    RUN apt-get install -y cron

        

WORKDIR /django

RUN mkdir staticfiles

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

COPY requirements.txt requirements.txt

# RUN python -m pip install --upgrade pip

RUN pip3 install -r requirements.txt


# copy entrypoint.sh
COPY entrypoint.sh entrypoint.sh

RUN chmod +x entrypoint.sh

# run entrypoint.sh to check if MySQL server is on before running application
ENTRYPOINT ["/django/entrypoint.sh"] 

# Copy hello-cron file to the cron.d directory

COPY my-crontab /etc/cron.d/my-crontab
COPY service/crowling.py crowling.py

# Give execution rights on the cron job
RUN chmod 0644 /etc/cron.d/my-crontab
RUN chmod a+x crowling.py

RUN crontab /etc/cron.d/my-crontab

# Create the log file to be able to run tail
RUN touch /var/log/cron.log
CMD ["cron", "-f"]
```

- We don't create docker files one by one. We will need to run everything at once.

### Running it all at once
Now that we have everything we need we will use *docker-compose* to fire everything up. The docker compose tells docker which services (with which images) to start and also sets the environment variables. 
#### Creating docker-compose.yml 
```
version: '3.9'

services:
  mysql:

  app:
    # Django application
  redis:
    # redis is used to manage background jobs in celery
  celery:
    # celery uses crontab to connect to django
  celery-beat:
    # to schedule cycling tasks
  nginx:
    # reverse proxy to handle request for django application
volumes:
  static_volume:
```

s
## RSYSLOG DOCKER LOGS:
To store Docker logs inside Rsyslog: 
* Configure daemon.json: ![](images/daemon-json.jpg)
* Create Docker config file: ![](images/docker-conf-log.jpg)
* Not Restarting rsyslog service and check results: ![](images/docker-log.jpg)
As you can see, successfully stored docker logs.


## Difficulties & Problems<a id="diff"> </a>
Following difficulties were faced in the whole process: 
1. Docker behaved differently on different machines and it raised some errors. When we tried to build docker-compose we have unsupported conf option healthcheck. 
**Solution** - In order to solve that problem, we installed the very same versions of docker, check docker-compose version and checked on different machines.
2. Cronjob background task was running the py crowling.py file but in py file, it was throwing error, reason is it was running seperated from django app so cannot connect to model for MySQl server through django.
**Solution** - We created celery and celery beat that uses cronjobs adjusted to django framework (also mostly used in production). with Celery beat we scheduled background task that crawles worldometers.com and scraps data from it and saves.
3. Sometimes MySQL server was delaying running when we start docker container and django application couldn't connect it on time and throwing error.
**Solution** - We add [entrypoint.sh](entrypoint.sh) to check MySQl database server by requesting if it is running before continuing to run django and other services in docker container. 

**When using Crontab itself to run py file for crowling and add data to database we faced with this problem below:**

![](images/cron-problem.jpg)

**We needed to use background job to save data through django, so we used Celery and Celery-beat to run every 10 minutes and got successfull result:**

![](images/background-success.jpg)


## Video Demonstration <a id="video"> </a>
[Video link](https://drive.google.com/file/d/11bpVRNPNE5bD2naIW3R2W6sLmmGEemGy/view?usp=sharing)

## Results <a id="res"> </a>

To run the app execute:
```$ docker-compose up -d --build```

1. Dockerfile:
![](images/success-docker.jpg)
![](images/success-docker2.jpg)
2. Check if all Images are successfully running:
![](images/docker-container.jpg)
3. To see image logs from docker-container:
![](images/docker-log.jpg)

4. Our application page view:
![](images/webpage1.jpg)
![](images/webpage2.jpg)


**CI/CD pipelines**

Here is the pipeline file:
[pipeline file](.gitlab-ci.yml)

## Conclusion<a id="conc"> </a>
We have been able to deploy a web application with the use of Docker Compose and for testing and deploying we implemented CI/CD pipeline. 

!NOTE: This Project is made smaller scale of real life application implementation, testing and deployment. Main focus is directed to DevOps side rather than application side to make use of our knowledge from SNA course. 

## Contribution <a id="cont"> </a>
* Shohjahon Khamrakulov [(@shohjahon_h)](https://t.me/shohjahon_h)
  * Created Django application and crowler
  * Dockerized whole application and nginx server
* Aibek Bakirov
  * Implemented Crontab for background job
* Davlatkhodzha Magzumov
  * Wrote report
* Nodir Bobiev
  * Implemented CI/CD pipeline
  * Testing


## Running <a id="runn"> </a>

* Build and Run: ```docker-compose up -d --build```

**Inside container: (If first time)**
```
docker exec -it my_project /bin/bash

# python3 manage.py collectstatic
```
run once to load styles for web app.

OR 

* Run itself: ```docker-compose up -d ```


