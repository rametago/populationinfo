from django.shortcuts import render
# Create your views here.
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
from .models import ByCountry, ByYear

def home(request):

    countries = ByCountry.objects.all()

    years = ByYear.objects.all()

    labels = []
    values = []

    for i in years:
        labels.append(i.year)
        values.append(i.population)

    

    return render(request, 'home.html', {'data':countries, 'labels':labels, 'values':values})
