# Generated by Django 3.2.3 on 2022-12-12 10:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0002_byyear'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='byyear',
            options={'ordering': ('year',)},
        ),
        migrations.AlterField(
            model_name='byyear',
            name='population',
            field=models.BigIntegerField(),
        ),
    ]
