import requests
import urllib
from bs4 import BeautifulSoup
from urllib.request import Request, urlopen


import os
import django


os.environ['DJANGO_SETTINGS_MODULE'] = 'core.settings'

django.setup()

from service.models import ByCountry, ByYear


def by_country():

    world_url = "https://www.worldometers.info/world-population/population-by-country/"
    data = []
    req = Request(world_url, headers={'User-Agent': 'Mozilla/5.0'})
    webpage = urlopen(req).read()
    soup2 = BeautifulSoup(webpage, 'html.parser')
    result = soup2.find('table', id = 'example2')
    table = result.find('tbody')
    rows = table.find_all('tr')

    for row in rows:
        cols = row.find_all('td')
        cols = [ele.text.strip() for ele in cols]
        data.append([ele for ele in cols])

    for item in data:
        if ByCountry.objects.filter(country=item[1]).exists():
            ByCountry.objects.filter(country=item[1]).update(country=item[1], population=item[2], yearly_change=item[3], net_change=item[4], density=item[5], land_area=item[6], migrants=item[7], fert_rate=item[8], med_age=item[9], urban_pop=item[10], world_share=item[11])
        else:
            ByCountry.objects.create(country=item[1], population=item[2], yearly_change=item[3], net_change=item[4], density=item[5], land_area=item[6], migrants=item[7], fert_rate=item[8], med_age=item[9], urban_pop=item[10], world_share=item[11])

    
    print("By Country is DONE!")



def by_year():

    url = "https://www.worldometers.info/world-population/world-population-by-year/"
    data = []
    req = Request(url, headers={'User-Agent': 'Mozilla/5.0'})
    webpage = urlopen(req).read()
    soup2 = BeautifulSoup(webpage, 'html.parser')
    result = soup2.find('table', class_ = 'table')
    table = result.find('tbody')

    rows = table.find_all('tr')
    

    for row in rows:
        cols = row.find_all('td')
        cols = [ele.text.strip() for ele in cols]
        data.append([ele for ele in cols])

    for item in data:
        year = int(item[0])
        population = int(item[1].replace(',',''))    

        if ByYear.objects.filter(year=year).exists():
            ByYear.objects.filter(year=year).update(year=year, population=population)
        else:
            ByYear.objects.create(year=year, population=population)

    print("By Year is Done!")
        











