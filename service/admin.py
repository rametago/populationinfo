from django.contrib import admin

# Register your models here.
from .models import ByCountry, ByYear

admin.site.register(ByCountry)
admin.site.register(ByYear)