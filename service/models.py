from django.db import models

# Create your models here.


class ByCountry(models.Model):
    country = models.CharField(max_length=100)
    population = models.CharField(max_length=100)
    yearly_change = models.CharField(max_length=100)
    net_change = models.CharField(max_length=100)
    density = models.CharField(max_length=100)
    land_area = models.CharField(max_length=100)
    migrants = models.CharField(max_length=100)
    fert_rate = models.CharField(max_length=100)
    med_age = models.CharField(max_length=100)
    urban_pop = models.CharField(max_length=100)
    world_share = models.CharField(max_length=100)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('-created_at',)

    def __str__(self):
        return self.country




class ByYear(models.Model):
    year = models.IntegerField()
    population= models.BigIntegerField()

    class Meta:
        ordering = ('year',)



